var board = [
    ['1', '2', '3'],
    ['4', '5', '6'],
    ['7', '8', null],
]

const reference = [
    ['1', '2', '3'],
    ['4', '5', '6'],
    ['7', '8', null],
]

var moves = 0

function shuffle(array, steps) {
    let temp
    for (let i = 0; i < steps; i++) {
        let a = Math.floor((Math.random() * 10) % 3)
        let b = Math.floor((Math.random() * 10) % 3)
        let c = Math.floor((Math.random() * 10) % 3)
        let d = Math.floor((Math.random() * 10) % 3)

        if (a != c && b != d && a != 9 && b != 9 && c != 9 && d != 9) {
            temp = array[a][b]
            array[a][b] = array[c][d]
            array[c][d] = temp
        }
    }
}
shuffle(board, 10)

function drawBoard(array) {
    let count = 0
    let span
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            span = document.getElementById(count).querySelector('span')
            span.innerHTML = array[i][j]
            count++
        }
    }
}

function cellClickHandle(event) {
    let index
    if (event.target.tagName == 'SPAN') {
        index = parseInt(event.target.parentElement.id) + 1
    } else {
        index = parseInt(event.target.id) + 1
    }
    let a = Math.ceil(index / 3) - 1
    let b
    if (index % 3 == 0) {
        b = 2
    } else {
        b = (index % 3) - 1
    }

    if (board[a][b]) {
        // identifies null cell
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (board[i][j] == null) {
                    var aNull = i
                    var bNull = j
                    break
                }
            }
        }

        // replaces cells
        let condition =
            ((Math.abs(a - aNull) == 1) ^ (Math.abs(b - bNull) == 1) &&
                (Math.abs(a - aNull) <= 1 && Math.abs(b - bNull)) <= 1) ||
            (a == aNull && Math.abs(b - bNull) == 2) ||
            (b == bNull && Math.abs(a - aNull) == 2)

        if (condition) {
            board[aNull][bNull] = board[a][b]
            board[a][b] = null
            drawBoard(board)
            moves++
            updateMoves()
            if (winCheck()) {
                alert('you won bitch')
                Reset()
            }
        }
    }
}

function winCheck() {
    let flag = true
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            if (board[i][j] != reference[i][j]) {
                flag = false
                break
            }
        }
    }
    return flag
}

function updateMoves() {
    document.getElementById('moves').innerHTML = moves
}

function Reset() {
    shuffle(board, 10)
    drawBoard(board)
    moves = 0
    updateMoves()
}

//adding click event to cells
var cells = document.getElementsByClassName('cell')
for (let i = 0; i < cells.length; i++) {
    cells[i].addEventListener('click', cellClickHandle)
}

//adding click event to reset button
document.querySelector('.btn').addEventListener('click', Reset)

drawBoard(board)
