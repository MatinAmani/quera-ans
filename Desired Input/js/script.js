document.getElementById('input').addEventListener('input', () => {
    var value = document.getElementById('input').value
    if (value < 0 || value > 10) {
        document.getElementById('input').style.backgroundColor = 'tomato'
    } else {
        document.getElementById('input').style.backgroundColor = 'white'
    }
})
