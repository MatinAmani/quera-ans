const CHANGE_TYPE = {
    UP: 'UP',
    DOWN: 'DOWN',
}
const ERROR_TYPE = {
    NOT_FOUND: 'NOT_FOUND',
    NOT_POSSIBLE: 'NOT_POSSIBLE',
    INVALID_INPUT: 'INVALID_INPUT',
}
let numbers = [4, 6, 10, 23, 0, 24, 30, 2]

// Your code here...

function createNodes(array) {
    for (let i = 0; i < array.length; i++) {
        let span = document.createElement('span')
        span.appendChild(document.createTextNode(array[i]))
        document.getElementById('numbers-container').appendChild(span)
    }
}

function updateNodes(array) {
    for (let i = 0; i < array.length; i++) {
        document.getElementById('numbers-container').children[0].remove()
    }
    for (let i = 0; i < array.length; i++) {
        let span = document.createElement('span')
        span.appendChild(document.createTextNode(array[i]))
        document.getElementById('numbers-container').appendChild(span)
    }
}

function manipulateArray() {
    if (validateInputs()) {
        // console.log('validation succeeded')
        let item = parseInt(document.getElementById('item-input').value)
        let count = parseInt(document.getElementById('count-input').value)
        let direction = document.getElementById('type-container').children[0]
            .checked
            ? 'UP'
            : 'DOWN'
        if (direction == 'UP') {
            let index = numbers.indexOf(item)
            for (let i = 0; i < count; i++) {
                let temp = numbers[index]
                numbers[index] = numbers[index + 1]
                numbers[index + 1] = temp
                index++
            }
        } else {
            let index = numbers.indexOf(item)
            for (let i = 0; i < count; i++) {
                let temp = numbers[index]
                numbers[index] = numbers[index - 1]
                numbers[index - 1] = temp
                index--
            }
        }
        console.log(item)
        updateNodes(numbers)
    }
}

function validateInputs() {
    let item = parseInt(document.getElementById('item-input').value)
    let count = parseInt(document.getElementById('count-input').value)
    if (item == '' && count) {
        if (numbers.includes(item)) {
            let pointer
            let index = numbers.indexOf(item)
            let direction = document.getElementById('type-container')
                .children[0].checked
                ? 'UP'
                : 'DOWN'
            if (direction == 'UP') {
                pointer = index + count
                if (pointer >= 0 && pointer <= numbers.length) {
                    deleteError()
                    return true
                } else {
                    appendError(ERROR_TYPE['NOT_POSSIBLE'])
                    return false
                }
            } else {
                pointer = index - count
                if (pointer >= 0 && pointer <= numbers.length) {
                    deleteError()
                    return true
                } else {
                    appendError(ERROR_TYPE['NOT_POSSIBLE'])
                    return false
                }
            }
        } else {
            appendError(ERROR_TYPE['NOT_FOUND'])
            return false
        }
    } else {
        appendError(ERROR_TYPE['INVALID_INPUT'])
        return false
    }
}

function appendError(type) {
    deleteError()
    let error = document.createElement('p')
    error.id = 'error'
    error.appendChild(document.createTextNode(type))
    document.getElementById('error-container').appendChild(error)
}

function deleteError() {
    if (document.getElementById('error')) {
        document.getElementById('error').remove()
    }
}

createNodes(numbers)
document.getElementById('submit-btn').addEventListener('click', manipulateArray)
