const statusDisplay = document.querySelector('.game--status')

let currentPlayer = 'X'
let gameState = ['', '', '', '', '', '', '', '', '']
let gameActive = true

const currentPlayerTurn = () => `It's ${currentPlayer}'s turn`

statusDisplay.innerHTML = currentPlayerTurn()

function handleCellPlayed(clickedCell, clickedCellIndex) {
    gameState[clickedCellIndex] = currentPlayer
    clickedCell.innerHTML = currentPlayer
}

function handlePlayerChange() {
    currentPlayer = currentPlayer === 'X' ? 'O' : 'X'
    statusDisplay.innerHTML = currentPlayerTurn()
}

function handleCellClick(clickedCellEvent) {
    const clickedCell = clickedCellEvent.target
    const clickedCellIndex = parseInt(
        clickedCell.getAttribute('data-cell-index')
    )

    if (gameState[clickedCellIndex] !== '') {
        return
    }

    // console.log(event.target)

    handleCellPlayed(clickedCell, clickedCellIndex)
    handlePlayerChange()
    winnerCheck()
}

function handleRestartGame() {
    currentPlayer = 'X'
    gameState = ['', '', '', '', '', '', '', '', '']
    statusDisplay.innerHTML = currentPlayerTurn()
    document.querySelectorAll('.cell').forEach((cell) => (cell.innerHTML = ''))
}

function winnerCheck() {
    let t = gameState
    let winner = ''

    //Row
    for (let i = 0; i <= 6; i = i + 3) {
        if (t[i] != '' && t[i] == t[i + 1] && t[i] == t[i + 2]) {
            winner = t[i]
            break
        }
    }

    //column
    for (let i = 0; i <= 2; i++) {
        if (t[i] != '' && t[i] == t[i + 3] && t[i] == t[i + 6]) {
            winner = t[i]
            break
        }
    }

    //Diagonal
    if (
        (t[0] != '' && t[0] == t[4] && t[0] == t[8]) ||
        (t[2] != '' && t[2] == t[4] && t[2] == t[6])
    ) {
        winner = t[4]
    }

    //tie
    let flag = true
    for (let i = 0; i < 9; i++) {
        if (t[i] == '') {
            flag = false
        }
    }
    if (flag && winner == '') {
        winner = 'tie'
    }

    if (winner && winner != 'tie') {
        alert('Player ' + winner + ' Won!')
    } else if (winner == 'tie') {
        alert("It's a tie!")
    }
}

document
    .querySelectorAll('.cell')
    .forEach((cell) => cell.addEventListener('click', handleCellClick))
document
    .querySelector('.game--restart')
    .addEventListener('click', handleRestartGame)
